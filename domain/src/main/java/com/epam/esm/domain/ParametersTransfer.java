package com.epam.esm.domain;

/**
 * Class used to transfer parameters from web
 * to service layer.
 *
 * @autor Tarelko Vitali
 */
public class ParametersTransfer {
    /**
     * Property - text attribute for searching
     */
    private String text;
    /**
     * Property - tag name for searching
     */
    private String tag;
    /**
     * Property - order parameter
     */
    private String order;

    /**
     * Return text property value
     */
    public String getText() {
        return text;
    }

    /**
     * Set text property value
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Return tag property value
     */
    public String getTag() {
        return tag;
    }

    /**
     * Set tag property value
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * Return order property value
     */
    public String getOrder() {
        return order;
    }

    /**
     * Set order property value
     */
    public void setOrder(String order) {
        this.order = order;
    }
}
