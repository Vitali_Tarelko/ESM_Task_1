package com.epam.esm.domain;

import com.epam.esm.domain.serializer.LocalDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * @autor Tarelko Vitali
 */
public class GiftCertificate {
    /**
     * Property - Unique certificate id.
     */
    private int id;
    /**
     * Property - Unique certificate name.
     */
    @NotBlank(message = "{cert.name.nonblank}")
    @Length(min = 2, max = 100, message = "{cert.name.length}")
    private String name;
    /**
     * Property - certificate description.
     */
    @NotBlank(message = "{cert.description.nonblank}")
    @Length(min = 2, max = 1000, message = "{cert.description.length}")
    private String description;
    /**
     * Property - price.
     */
    @NotNull(message = "{price.notnull}")
    @Max(value = 999999, message = "{price.max}")
    @Min(value = 1, message = "{price.min}")
    @Digits(integer = 6, fraction = 2, message = "{price.digits}")
    private BigDecimal price;
    /**
     * Property - date of certificate's creation.
     */
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate creationDate;
    /**
     * Property - date of certificate's modification.
     */
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate modificationDate;
    /**
     * Property - duration, count days till expiration date.
     */
    @NotNull(message = "{duration.notnull}")
    @Min(value = 10, message = "{duration.min}")
    @Max(value = 365, message = "{duration.max}")
    private int duration;

    private List<Tag> tagList;

    /**
     * Default constructor.
     */
    public GiftCertificate() {
    }
    /**
     * Constructor - creating a new object with parameters.
     *
     * @param builder -  inner class, used to create
     *                needed query and add parameters in array.
     */
    public GiftCertificate(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.description = builder.description;
        this.price = builder.price;
        this.creationDate = builder.creationDate;
        this.modificationDate = builder.modificationDate;
        this.duration = builder.duration;
        this.tagList = builder.tagList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        GiftCertificate that = (GiftCertificate) o;
        return id == that.id &&
                duration == that.duration &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(price, that.price) &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(modificationDate, that.modificationDate) &&
                Objects.equals(tagList, that.tagList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, creationDate, modificationDate, duration, tagList);
    }

    @Override
    public String toString() {
        return "GiftCertificate{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                ", duration=" + duration +
                ", tagList=" + tagList +
                '}';
    }

    public static class Builder {
        private int id;
        private String name;
        private String description;
        private BigDecimal price;
        private LocalDate creationDate;
        private LocalDate modificationDate;
        private int duration;
        private List<Tag> tagList;

        public Builder(String name) {
            this.name = name;
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withPrice(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder withCreationDate(LocalDate creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public Builder withModificationDate(LocalDate modificationDate) {
            this.modificationDate = modificationDate;
            return this;
        }

        public Builder withDuration(int duration) {
            this.duration = duration;
            return this;
        }

        public Builder withTags(List<Tag> tags) {
            this.tagList = tags;
            return this;
        }

        public GiftCertificate buildCertificate() {
            return new GiftCertificate(this);
        }


    }

}
