package com.epam.esm.domain;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

/**
 * @autor Tarelko Vitali
 */
public class Tag {
    /**
     * Property - Unique tag id.
     */
    private int id;
    @NotBlank(message = "{tag.name.notblank}")
    @Length(min = 1, max = 20, message = "{tag.name.length}")
    /**
     * Property - Unique name.
     */
    private String name;

    /**
     * Default constructor.
     */
    public Tag() {
    }

    /**
     * Constructor - creating a new object with parameters.
     *
     * @param id -  Unique tag id.
     */
    public Tag(int id) {
        this.id = id;
    }

    /**
     * Constructor - creating a new object with parameters.
     *
     * @param name - tag name.
     */

    public Tag(String name) {
        this.name = name;
    }

    /**
     * Constructor - creating a new object with parameters.
     *
     * @param id   -  tag id.
     * @param name - tag name.
     */

    public Tag(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Return id property
     */
    public int getId() {
        return id;
    }

    /**
     * Set id property
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Return name property
     */
    public String getName() {
        return name;
    }

    /**
     * Set name property
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return id == tag.id &&
                Objects.equals(name, tag.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
