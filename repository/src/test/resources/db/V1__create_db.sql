create SCHEMA "GiftCertificates";

CREATE TABLE "GiftCertificates".tags
(
    id serial NOT NULL,
    name text COLLATE pg_catalog."default",
    CONSTRAINT tags_pkey PRIMARY KEY (id),
    CONSTRAINT name UNIQUE (name)
);

CREATE TABLE "GiftCertificates".certificates
(
    id serial NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default",
    price numeric(8,2) NOT NULL,
    creation_date date NOT NULL,
    modification_date date NOT NULL,
    duration integer NOT NULL,
    CONSTRAINT certificates_pkey PRIMARY KEY (id),
    CONSTRAINT certificates_name_key UNIQUE (name)
);

CREATE TABLE "GiftCertificates".tag_certificate
(
    tag_id integer NOT NULL,
    certificate_id integer NOT NULL,
    CONSTRAINT pk PRIMARY KEY (tag_id, certificate_id),
    CONSTRAINT tag_certificate_tag_id_fkey FOREIGN KEY (tag_id)
        REFERENCES "GiftCertificates".tags (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE cascade,
    CONSTRAINT tag_certificate_certificate_id_fkey FOREIGN KEY (certificate_id)
        REFERENCES "GiftCertificates".certificates (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE cascade
);

CREATE  OR REPLACE FUNCTION "GiftCertificates".find_by_text (string text)
RETURNS TABLE (id integer, name text, description text, price numeric, creation_date date, modification_date date, duration integer, tag_name text[], tag_id integer[])
AS $$
        BEGIN
               RETURN QUERY Select "GiftCertificates".certificates.id, "GiftCertificates".certificates.name, "GiftCertificates".certificates.description,
			   "GiftCertificates".certificates.price, "GiftCertificates".certificates.creation_date,
			   "GiftCertificates".certificates.modification_date, "GiftCertificates".certificates.duration,
			   array_agg("GiftCertificates".tags.name),  array_agg("GiftCertificates".tags.id)
			   from "GiftCertificates".certificates left join "GiftCertificates".tag_certificate on certificate_id="GiftCertificates".certificates.id
			   LEFT JOIN "GiftCertificates".tags on "GiftCertificates".tags.id="GiftCertificates".tag_certificate.tag_id
			   where "GiftCertificates".certificates.name  ILIKE '%' ||  $1 || '%'
			   OR "GiftCertificates".certificates.description  ILIKE '%' ||  $1 || '%'
			   GROUP BY "GiftCertificates".certificates.id;
        END;
$$ LANGUAGE plpgsql;
CREATE  OR REPLACE FUNCTION "GiftCertificates".find_by_text_and_tag (string text, tag text)
RETURNS TABLE (id integer, name text, description text, price numeric, creation_date date, modification_date date, duration integer, tag_name text[], tag_id integer[])
AS $$
        BEGIN
               RETURN QUERY Select "GiftCertificates".certificates.id, "GiftCertificates".certificates.name, "GiftCertificates".certificates.description,
			   "GiftCertificates".certificates.price, "GiftCertificates".certificates.creation_date,
			   "GiftCertificates".certificates.modification_date, "GiftCertificates".certificates.duration,
			   array_agg("GiftCertificates".tags.name),  array_agg("GiftCertificates".tags.id)
			from "GiftCertificates".certificates left join "GiftCertificates".tag_certificate on certificate_id="GiftCertificates".certificates.id
			   LEFT JOIN "GiftCertificates".tags on "GiftCertificates".tags.id="GiftCertificates".tag_certificate.tag_id
			   where "GiftCertificates".tags.name = $2 and ("GiftCertificates".certificates.name  ILIKE '%' ||  $1 || '%'
			   OR "GiftCertificates".certificates.description  ILIKE '%' ||  $1 || '%' )
			   GROUP BY "GiftCertificates".certificates.id;
        END;
$$ LANGUAGE plpgsql;


CREATE  OR REPLACE FUNCTION "GiftCertificates".find_by_tag (string text)
RETURNS TABLE (id integer, name text, description text, price numeric, creation_date date, modification_date date, duration integer, tag_name text[], tag_id integer[])
AS $$
        BEGIN
               RETURN QUERY Select "GiftCertificates".certificates.id, "GiftCertificates".certificates.name, "GiftCertificates".certificates.description,
			   "GiftCertificates".certificates.price, "GiftCertificates".certificates.creation_date,
			   "GiftCertificates".certificates.modification_date, "GiftCertificates".certificates.duration,
			   array_agg("GiftCertificates".tags.name),  array_agg("GiftCertificates".tags.id)
			   from "GiftCertificates".certificates left join "GiftCertificates".tag_certificate on certificate_id="GiftCertificates".certificates.id
			   LEFT JOIN "GiftCertificates".tags on "GiftCertificates".tags.id="GiftCertificates".tag_certificate.tag_id
			   where "GiftCertificates".tags.name = $1
			   GROUP BY "GiftCertificates".certificates.id;
        END;
$$ LANGUAGE plpgsql;