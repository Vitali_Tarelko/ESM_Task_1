package com.epam.esm.repository;

import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.repository.specification.CertificateSpecification;
import com.opentable.db.postgres.embedded.FlywayPreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class GiftCertificateRepositoryTest {

    private static final Logger LOGGER = LogManager.getLogger(GiftCertificateRepositoryTest.class);
    @Rule
    public PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db"));
    private AbstractRepository repository;
    private List<GiftCertificate> testData;

    @Before
    public void setUp() {
        repository = new GiftCertificateRepository((new JdbcTemplate(db.getTestDatabase())));
        testData = new ArrayList<>();
        testData.add(new GiftCertificate.Builder("egestas. Aliquam fringilla cursus purus.")
                .withDescription("convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc")
                .withId(1)
                .withDuration(67)
                .withPrice(new BigDecimal(5.10))
                .withCreationDate(LocalDate.of(2018, Month.FEBRUARY, 5))
                .withModificationDate(LocalDate.of(2018, Month.MARCH, 5))
                .buildCertificate());
        testData.add(new GiftCertificate.Builder("NewGiftCertificate")
                .withDescription("Some description here")
                .withId(100)
                .withDuration(67)
                .withPrice(new BigDecimal(5.10))
                .withCreationDate(LocalDate.of(2018, Month.FEBRUARY, 5))
                .withModificationDate(LocalDate.of(2018, Month.MARCH, 5))
                .buildCertificate());
    }

    @After
    public void tearDown() {
        repository = null;
        testData = null;
    }

    @Test
    public void createTest() {
        LOGGER.info("========Creating new record in DB========");
        int id = repository.create(testData.get(1));
        LOGGER.info("Expected result " + 11 + " | Result is: " + id);
        assertEquals(11, id);
    }

    @Test(expected = DuplicateKeyException.class)
    public void createTestDuplicateTest() {
        LOGGER.info("========Creating new record in DB Error========");
        repository.create(testData.get(0));
    }


    @Test
    public void remove() {
        LOGGER.info("========Remove Certificate by ID========");
        boolean result = repository.remove(3);
        assertEquals(true, result);
    }

    @Test
    public void removeNotExistIdTest() {
        LOGGER.info("========Remove Certificate by ID ERROR========");
        boolean result = repository.remove(50);
        assertEquals(false, result);
    }

    @Test
    public void update() {
        LOGGER.info("========Update Certificate========");
        GiftCertificate certificate = testData.get(0);
        certificate.setName("UpdatedName");
        boolean result = repository.update(certificate);
        LOGGER.info("Expected result: " + true + " | Result is: " + result);
        assertEquals(true, result);
        GiftCertificate updated =
                (GiftCertificate) repository.query(new CertificateSpecification.QueryBuilder().byId(certificate.getId()).groupBy().buildQuery()).get(0);
        LOGGER.info("Result: " + updated.toString());
    }

    @Test
    public void updateNotExistIdTest() {
        LOGGER.info("========Update Certificate Error========");
        boolean result = repository.update(testData.get(1));
        LOGGER.info("Expected result: " + false + " | Result is: " + result);
        assertEquals(false, result);
    }

    @Test
    public void getAllTest() {
        List<GiftCertificate> result = repository.query(new CertificateSpecification.QueryBuilder().groupBy().buildQuery());
        result.forEach(certificate -> LOGGER.info(certificate.toString()));
        assertEquals(10, result.size());
    }
    @Test
    public void getByTagName() {
        List<GiftCertificate> result = repository.query(new CertificateSpecification.QueryBuilder().byTag("mi").buildQuery());
        result.forEach(certificate -> LOGGER.info(certificate.toString()));
        assertEquals(5, result.size());
    }
    @Test
    public void getByTextName() {
        List<GiftCertificate> result = repository.query(new CertificateSpecification.QueryBuilder().byText("mi").buildQuery());
        result.forEach(certificate -> LOGGER.info(certificate.toString()));
        assertEquals(1, result.size());
    }
    @Test
    public void getByTextAndTagName() {
        List<GiftCertificate> result = repository.query(new CertificateSpecification.QueryBuilder().byTagAndText("mi", "Nulla").buildQuery());
        result.forEach(certificate -> LOGGER.info(certificate.toString()));
        assertEquals(1, result.size());
    }
    @Test
    public void getAllOrderName() {
        List<GiftCertificate> result = repository.query(new CertificateSpecification.QueryBuilder().groupBy().orderByName().buildQuery());
        result.forEach(certificate -> LOGGER.info(certificate.toString()));
        assertEquals("ante, iaculis nec, eleifend non,", result.get(0).getName());
    }
    @Test
    public void getAllOrderDate() {
        List<GiftCertificate> result = repository.query(new CertificateSpecification.QueryBuilder().groupBy().orderByModificationDate().buildQuery());
        result.forEach(certificate -> LOGGER.info(certificate.toString()));
        assertEquals("Maecenas malesuada fringilla est. Mauris", result.get(0).getName());
    }
}