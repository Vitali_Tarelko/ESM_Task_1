package com.epam.esm.repository;

import com.epam.esm.domain.Tag;
import com.epam.esm.repository.specification.TagSpecification;
import com.opentable.db.postgres.embedded.FlywayPreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TagRepositoryTest {

    private static final Logger LOGGER = LogManager.getLogger(TagRepositoryTest.class);
    @Rule
    public PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db"));
    private TagRepository repository;
    private List<Tag> testData;

    @Before
    public void setUp() {
        repository = new TagRepository(new JdbcTemplate(db.getTestDatabase()));
        testData = new ArrayList<>();
        testData.add(new Tag(1, "malesuada"));
        testData.add(new Tag(3, "diam"));
        testData.add(new Tag(15, "New_Tag"));
    }

    @After
    public void tearDown() {
        repository = null;
        testData = null;
    }

    @Test
    public void createTest() {
        LOGGER.info("========Creating new record in DB========");
        int result = repository.create(testData.get(2));
        LOGGER.info("Expected result " + 11 + " | Result is: " + result);
        assertEquals(11, result);
    }

    @Test(expected = DuplicateKeyException.class)
    public void createTestDuplicateTest() {
        LOGGER.info("========Creating new record in DB Error========");
        repository.create(testData.get(0));
    }

    @Test
    public void remove() {
        LOGGER.info("========Remove Tag by ID========");
        boolean result = repository.remove(3);
        assertEquals(true, result);
    }

    @Test
    public void removeNotExistIdTest() {
        LOGGER.info("========Remove Tag by ID ERROR========");
        boolean result = repository.remove(50);
        assertEquals(false, result);
    }

    @Test
    public void update() {
        LOGGER.info("========Update Tag========");
        Tag tag = testData.get(1);
        tag.setName("newName");
        boolean result = repository.update(tag);
        LOGGER.info("Expected result: " + true + " | Result is: " + result);
        assertEquals(true, result);
    }

    @Test
    public void updateNotExistIdTest() {
        LOGGER.info("========Update Tag Error========");
        boolean result = repository.update(testData.get(2));
        LOGGER.info("Expected result: " + false + " | Result is: " + result);
        assertEquals(false, result);
    }

    @Test
    public void getAllTest() {
        List<Tag> result = repository.query(new TagSpecification.QueryBuilder().buildQuery());
        result.forEach(tag -> LOGGER.info(tag.toString()));
        assertEquals(10, result.size());
    }

    @Test
    public void getByIdTagTest() {
        Tag expected = testData.get(0);
        List<Tag> resultList =
                repository.query(new TagSpecification.QueryBuilder().byId(expected.getId()).buildQuery());
        LOGGER.info("Expected Tag: " + expected.toString() + ", Result is: " + resultList.get(0).toString());
        assertEquals(expected, resultList.get(0));
    }

    @Test
    public void getByNameTagTest() {
        Tag expected = testData.get(1);
        List<Tag> resultList = repository.query(new TagSpecification.QueryBuilder().byName("diam").buildQuery());
        LOGGER.info("Expected Tag: " + expected.toString() + ", Result is: " + resultList.get(0).toString());
        assertEquals(expected, resultList.get(0));
    }

    @Test
    public void getByCertificateIdTest() {
        List<Tag> resultList = repository.query(new TagSpecification.QueryBuilder().byCertificateId(2).buildQuery());
        LOGGER.info("Expected List size: " + 3 + ", Result is: " + resultList.size());
        assertEquals(3, resultList.size());
    }

    @Test
    public void getByIdAndNameTest() {
        List<Tag> resultList =
                repository.query(new TagSpecification.QueryBuilder().byId(1).byName("malesuada").buildQuery());
        LOGGER.info("Expected List size: " + 1 + ", Result is: " + resultList.size());
        assertEquals(1, resultList.size());
    }

    @Test
    public void getByNameAndITestNoy() {
        List<Tag> resultList =
                repository.query(new TagSpecification.QueryBuilder().byName("diam").byId(3).buildQuery());
        LOGGER.info("Expected List size: " + 1 + ", Result is: " + resultList.size());
        assertEquals(1, resultList.size());
    }

    @Test
    public void removeTadCertificateDependency() {
        boolean result = repository.removeTagCertificateBy(3);
        assertEquals(true, result);
        List<Tag> resultList = repository.query(new TagSpecification.QueryBuilder().byCertificateId(3).buildQuery());
        LOGGER.info("Expected List size: " + 0 + ", Result is: " + resultList.size());
        assertEquals(0, resultList.size());
    }

    @Test
    public void createTadCertificateDependency() {
        repository.createDependency(3, 1);
        List<Tag> resultList = repository.query(new TagSpecification.QueryBuilder().byCertificateId(1).buildQuery());
        LOGGER.info("Expected List size: " + 4 + ", Result is: " + resultList.size());
        assertEquals(4, resultList.size());
    }
}