package com.epam.esm.repository.mapper;

import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.domain.Tag;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GiftCertificateMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        List<Tag> tagList = new ArrayList<>();
        Array tagName = resultSet.getArray("tag_name");
        Array tagId = resultSet.getArray("tag_id");
        if (!tagName.toString().equals("{NULL}")) {
            Integer[] tagIdArray = (Integer[]) tagId.getArray();
            String[] tagNameArray = (String[]) tagName.getArray();
            for (int a = 0; a < tagIdArray.length; a++) {
                tagList.add(new Tag(tagIdArray[a], tagNameArray[a]));
            }
        }
        return new GiftCertificate.Builder(resultSet.getString("name"))
                .withId(resultSet.getInt("id"))
                .withPrice(resultSet.getBigDecimal("price"))
                .withDescription(resultSet.getString("description"))
                .withCreationDate(resultSet.getDate("creation_date").toLocalDate())
                .withModificationDate(resultSet.getDate("modification_date").toLocalDate())
                .withDuration(resultSet.getInt("duration"))
                .withTags(tagList).buildCertificate();
    }
}
