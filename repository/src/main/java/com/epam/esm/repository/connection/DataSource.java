package com.epam.esm.repository.connection;

import org.springframework.jdbc.datasource.AbstractDataSource;

import java.sql.Connection;

public class DataSource extends AbstractDataSource {
    private ConnectionPool pool;

    public DataSource(ConnectionPool pool) {
        this.pool = pool;
    }

    @Override
    public Connection getConnection() {
            return pool.getConnection();
    }

    @Override
    public Connection getConnection(String username, String password) {
        throw new UnsupportedOperationException("Unsupported operation");
    }
}
