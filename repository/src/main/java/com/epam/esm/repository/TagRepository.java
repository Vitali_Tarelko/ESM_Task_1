package com.epam.esm.repository;

import com.epam.esm.domain.Tag;
import com.epam.esm.repository.mapper.TagMapper;
import com.epam.esm.repository.specification.Specification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagRepository implements AbstractRepository<Tag, Integer> {

    private static final String INSERT_QUERY = "INSERT INTO \"GiftCertificates\".tags (name) VALUES (?) RETURNING id";
    private static final String DELETE_QUERY = "DELETE FROM \"GiftCertificates\".tags WHERE id = ?";
    private static final String UPDATE_QUERY = "UPDATE \"GiftCertificates\".tags SET name = ? WHERE id = ?";
    private static final String INSERT_DEPENDENCY_QUERY = "INSERT INTO \"GiftCertificates\".tag_certificate(tag_id, " +
            "certificate_id) VALUES (?, ?);";
    private static final String DELETE_DEPENDENCY_QUERY = "DELETE FROM \"GiftCertificates\".tag_certificate WHERE " +
            "certificate_id=?;";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TagRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int create(Tag entity) {
        return jdbcTemplate.queryForObject(INSERT_QUERY, new Object[]{entity.getName()}, Integer.class);
    }

    @Override
    public boolean remove(Integer id) {
        return jdbcTemplate.update(DELETE_QUERY, id) > 0 ? true : false;
    }

    @Override
    public boolean update(Tag entity) {
        return jdbcTemplate.update(UPDATE_QUERY, entity.getName(), entity.getId()) > 0 ? true : false;
    }

    @Override
    public List<Tag> query(Specification specification) {
        return jdbcTemplate.query(specification.getQuery(), specification.getParameters(), new TagMapper());
    }

    public void createDependency(int tagId, int certificateId) {
        jdbcTemplate.update(INSERT_DEPENDENCY_QUERY, tagId, certificateId);
    }

    public boolean removeTagCertificateBy(int id) {
        return jdbcTemplate.update(DELETE_DEPENDENCY_QUERY, id) > 0 ? true : false;
    }
}
