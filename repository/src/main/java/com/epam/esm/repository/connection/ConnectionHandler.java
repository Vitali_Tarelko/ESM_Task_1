package com.epam.esm.repository.connection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionHandler implements InvocationHandler {

    private static final String METHOD_NAME = "close";
    private Connection connection;

    public ConnectionHandler(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws SQLException, InterruptedException,
            InvocationTargetException, IllegalAccessException {
        if (method.getName().equals(METHOD_NAME)) {
            ConnectionPool.getInstance().returnConnection(connection);
            return null;
        }
        return method.invoke(connection, args);
    }
}
