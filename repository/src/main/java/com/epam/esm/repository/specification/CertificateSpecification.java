package com.epam.esm.repository.specification;

import java.util.ArrayList;
import java.util.List;

/**
 * @autor Tarelko Vitali
 * @see com.epam.esm.repository.specification.Specification
 */
public class CertificateSpecification implements Specification {
    /**
     * Property - sql query.
     */
    private String query;
    /**
     * Property - array of object's.
     */
    private Object[] parameters;

    /**
     * Constructor - creating a new object with parameters.
     *
     * @param builder -  nested class, used to create
     *                needed query and add parameters in array.
     */
    public CertificateSpecification(QueryBuilder builder) {
        this.query = builder.query;
        this.parameters = builder.parameters.toArray();
    }

    /**
     * @see com.epam.esm.repository.specification.Specification
     */
    @Override
    public String getQuery() {
        return query;
    }

    /**
     * @see com.epam.esm.repository.specification.Specification
     */
    @Override
    public Object[] getParameters() {
        return parameters;
    }

    /**
     * Static nested class, used to create sql specified sql query
     * and add parameters in list.
     */
    public static class QueryBuilder {
        private static final String WHERE_CONDITION = "WHERE";
        /**
         * Property - default sql query. Select all records from database
         */
        private String query = "Select  \"GiftCertificates\".certificates.id, \"GiftCertificates\".certificates.name," +
                " " +
                "description, " +
                "price, creation_date, modification_date, duration,\n" +
                "array_agg(\"GiftCertificates\".tags.name) AS tag_name,\n" +
                "array_agg(\"GiftCertificates\".tags.id) AS tag_id \n" +
                "FROM \"GiftCertificates\".certificates \n" +
                "LEFT JOIN \"GiftCertificates\".tag_certificate on certificate_id=id \n" +
                "LEFT JOIN \"GiftCertificates\".tags on \"GiftCertificates\".tags.id=tag_id ";
        /**
         * Property - default sql query for call function.
         */
        private String queryForFunction = "select id, name,description,price,creation_date,modification_date," +
                "duration," +
                "tag_name, tag_id from ";
        /**
         * Property - List with parameters
         */
        private List<Object> parameters = new ArrayList<>();

        /**
         * Method add condition for searching by ID
         * to existing query and add parameter in list.
         *
         * @param id is an object that will be added to parameters list.
         * @return current state of QueryBuilder object.
         */
        public QueryBuilder byId(int id) {
            if (!query.contains(WHERE_CONDITION)) {
                query += " WHERE \"GiftCertificates\".certificates.id=? ";
            } else {
                query += "AND \"GiftCertificates\".certificates.id=? ";
            }
            this.parameters.add(id);
            return this;
        }

        /**
         * Method add condition for call function
         * "find_by_text"
         * to existing query and add parameter in list.
         *
         * @param text is an object that will be added to parameters list.
         * @return current state of QueryBuilder object.
         */
        public QueryBuilder byText(String text) {
            query = queryForFunction + "\"GiftCertificates\".find_by_text(?)";
            parameters.clear();
            parameters.add(text);
            return this;
        }

        /**
         * Method add condition for call function
         * "find_by_text_and_tag"
         * to existing query and add parameter in list.
         *
         * @param text    is an object that will be added to parameters list.
         * @param tagName is an object that will be added to parameters list.
         * @return current state of QueryBuilder object.
         */
        public QueryBuilder byTagAndText(String text, String tagName) {
            query = queryForFunction + "\"GiftCertificates\".find_by_text_and_tag(?,?) ";
            parameters.clear();
            parameters.add(text);
            parameters.add(tagName);
            return this;
        }

        /**
         * Method add condition for call function
         * "find_by_tag"
         * to existing query and add parameter in list.
         *
         * @param tagName is an object that will be added to parameters list.
         * @return current state of QueryBuilder object.
         */
        public QueryBuilder byTag(String tagName) {
            query = queryForFunction + "\"GiftCertificates\".find_by_tag(?) ";
            parameters.clear();
            parameters.add(tagName);
            return this;
        }

        /**
         * Method add "GROUP BY" condition to query.
         *
         * @return current state of QueryBuilder object.
         */
        public QueryBuilder groupBy() {
            query += " GROUP BY \"GiftCertificates\".certificates.id";
            return this;
        }

        /**
         * Method add "ORDER BY name" condition to query.
         *
         * @return current state of QueryBuilder object.
         */
        public QueryBuilder orderByName() {
            query += " ORDER BY name ";
            return this;
        }

        /**
         * Method add "ORDER BY modification_date" condition to query.
         *
         * @return current state of QueryBuilder object.
         */
        public QueryBuilder orderByModificationDate() {
            query += " ORDER BY modification_date ";
            return this;
        }

        /**
         * Method create a new CertificateSpecification object with current sql query
         * and array of parameters
         *
         * @return new CertificateSpecification object.
         */
        public CertificateSpecification buildQuery() {
            return new CertificateSpecification(this);
        }

    }


}
