package com.epam.esm.repository;


import com.epam.esm.repository.connection.ConnectionPool;
import com.epam.esm.repository.connection.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;


@Configuration
@ComponentScan(basePackages = "com.epam.esm")
public class RepositoryConfig {


    @Bean
    public DataSource dataSource() {
        return new DataSource(ConnectionPool.getInstance());
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }
}
