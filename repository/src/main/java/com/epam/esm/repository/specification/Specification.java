package com.epam.esm.repository.specification;

/**
 * The interface is used to prepare data
 * for jdbcTemplate query.
 *
 * @autor Tarelko Vitali
 */
public interface Specification {

    /**
     * Method return sql query
     *
     * @return string query
     */
    String getQuery();

    /**
     * Method returns array of objects for jdbcTemplate
     *
     * @return array with parameters for query
     */
    Object[] getParameters();

}
