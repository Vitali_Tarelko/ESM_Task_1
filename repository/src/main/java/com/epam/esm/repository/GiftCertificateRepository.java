package com.epam.esm.repository;

import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.repository.mapper.GiftCertificateMapper;
import com.epam.esm.repository.specification.Specification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GiftCertificateRepository implements AbstractRepository<GiftCertificate, Integer> {

    private static final String INSERT_QUERY = "INSERT INTO \"GiftCertificates\".certificates (name, description, " +
            "price, creation_date, modification_date, duration) VALUES (?,?,?,?,?,?) RETURNING id";
    private static final String DELETE_QUERY = "DELETE FROM \"GiftCertificates\".certificates WHERE id = ?";
    private static final String UPDATE_QUERY = "UPDATE \"GiftCertificates\".certificates SET name=?, description=?, " +
            "price=?, creation_date=?, modification_date=?, duration=? WHERE id = ?";
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public GiftCertificateRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int create(GiftCertificate entity) {
        return jdbcTemplate.queryForObject(INSERT_QUERY, new Object[]{entity.getName(), entity.getDescription(),
                        entity.getPrice(), entity.getCreationDate(), entity.getModificationDate(), entity.getDuration
                        ()},
                Integer.class);
    }

    public boolean remove(Integer id) {
        return jdbcTemplate.update(DELETE_QUERY, id) > 0 ? true : false;
    }

    public boolean update(GiftCertificate entity) {
        return jdbcTemplate.update(UPDATE_QUERY, entity.getName(), entity.getDescription(), entity.getPrice(), entity
                .getCreationDate(), entity.getModificationDate(), entity.getDuration(), entity.getId()) > 0 ? true :
                false;

    }

    public List<GiftCertificate> query(Specification specification) {
        return jdbcTemplate.query(specification.getQuery(), specification.getParameters(), new GiftCertificateMapper());
    }

    @Override
    public boolean removeTagCertificateBy(int id) {
        throw new UnsupportedOperationException("This operation is not supported in for Certificate");
    }

    @Override
    public void createDependency(int id, int id1) {
        throw new UnsupportedOperationException("This operation is not supported in for Certificate");
    }
}
