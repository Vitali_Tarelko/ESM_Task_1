package com.epam.esm.repository.specification;

import java.util.ArrayList;
import java.util.List;

/**
 * @autor Tarelko Vitali
 * @see com.epam.esm.repository.specification.Specification
 */
public class TagSpecification implements Specification {
    /**
     * Property - sql query.
     */
    private String query;
    /**
     * Property - array of object's.
     */
    private Object[] parameters;

    /**
     * Constructor - creating a new object with parameters.
     *
     * @param builder -  nested class, used to create
     *                needed query and add parameters in array.
     */
    public TagSpecification(QueryBuilder builder) {
        this.query = builder.query;
        this.parameters = builder.parameters.toArray();
    }

    /**
     * @see com.epam.esm.repository.specification.Specification
     */
    public String getQuery() {
        return query;
    }

    /**
     * @see com.epam.esm.repository.specification.Specification
     */
    public Object[] getParameters() {
        return parameters;
    }

    /**
     * Static nested class, used to create sql specified sql query
     * and add parameters in list.
     */
    public static class QueryBuilder {
        private static final String WHERE_CONDITION = "WHERE";
        /**
         * Property - default sql query. Select all records from database
         */
        private String query = "Select id, name from \"GiftCertificates\".tags ";
        /**
         * Property - List with parameters
         */
        private List<Object> parameters = new ArrayList<>();

        /**
         * Method add condition for searching by ID
         * to existing query and add parameter in list.
         *
         * @param id is an object that will be added to parameters list.
         * @return current state of QueryBuilder object.
         */
        public QueryBuilder byId(int id) {
            if (!query.contains(WHERE_CONDITION)) {
                query += " WHERE id=? ";
            } else {
                query += "AND id=? ";
            }
            this.parameters.add(id);
            return this;
        }

        /**
         * Method add condition for searching by Name
         * to existing query and add parameter in list.
         *
         * @param name is an object that will be added to parameters list.
         * @return current state of QueryBuilder object.
         */
        public QueryBuilder byName(String name) {
            if (!query.contains(WHERE_CONDITION)) {
                query += " WHERE name=? ";
            } else {
                query += "AND name=? ";
            }
            this.parameters.add(name);
            return this;
        }

        /**
         * Method add condition for searching by Certificate ID in
         * tag_certificate table to existing query and add parameter in list.
         * Used only with default constructor. Not after searching byId or Name!
         *
         * @param certificateId is an object that will be added to parameters list.
         * @return current state of QueryBuilder object.
         */
        public QueryBuilder byCertificateId(int certificateId) {
            if (!query.contains(WHERE_CONDITION)) {
                query += "INNER JOIN \"GiftCertificates\".tag_certificate tc ON id = tc.tag_id where tc" +
                        ".certificate_id =?";
            } else {
                query = query.split(WHERE_CONDITION)[0] + "INNER JOIN \"GiftCertificates\".tag_certificate tc ON id =" +
                        " tc" +
                        ".tag_id where tc.certificate_id =?";
            }
            this.parameters.clear();
            this.parameters.add(certificateId);
            return this;
        }

        /**
         * Method create a new TagSpecification object with current sql query
         * and array of parameters
         *
         * @return new TagSpecification object.
         */
        public TagSpecification buildQuery() {
            return new TagSpecification(this);
        }


    }


}
