package com.epam.esm.repository;

import com.epam.esm.repository.specification.Specification;

import java.util.List;

/**
 * Interface for CRUD operations on a repository for a specific type.
 *
 * @autor Tarelko Vitali
 */

public interface AbstractRepository<E, K> {
    /**
     * Method add a new record in database.
     *
     * @param entity is an object that will be added to the database.
     * @return true if the record was added successful. Otherwise false.
     */
    int create(E entity);

    /**
     * Method delete existing record in database.
     *
     * @param id is  primary key in table.
     * @return true if the record was deleted successful. Otherwise false.
     */
    boolean remove(K id);

    /**
     * Method update a existing record in database.
     *
     * @param entity is an object that will be updated in the database.
     * @return true if the record was updated successful. Otherwise false.
     */
    boolean update(E entity);

    /**
     * Method for reading records in database.
     *
     * @param specification is an object that return query and parameters for searching data.
     * @return specific type object list.
     * @see com.epam.esm.repository.specification.Specification
     */
    List<E> query(Specification specification);

    /**
     * Method delete existing record in database.
     *
     * @param id is  giftCertificate key in table.
     * @return true if the record was deleted successful. Otherwise false.
     */
    boolean removeTagCertificateBy(int id);

    /**
     * Method create new record in database.
     *
     * @param id  is  tag primary key
     * @param id1 is giftCertificate primary key.
     */
    void createDependency(int id, int id1);
}
