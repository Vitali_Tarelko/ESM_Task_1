package com.epam.esm.repository.connection;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.postgresql.Driver;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;


public class ConnectionPool {

    private static ReentrantLock lock = new ReentrantLock();
    private static ConnectionPool instance = new ConnectionPool();
    private Logger logger = LogManager.getLogger();
    private String jdbcUrl;
    private int poolSize;
    private String userName;
    private String password;
    private BlockingQueue<Connection> connections;


    private ConnectionPool() {
        paramInit();
        try {
            DriverManager.registerDriver(new Driver());
            connections = new LinkedBlockingQueue<>(poolSize);
            for (int i = 0; i < poolSize; i++) {
                connections.put(newConnection());
            }
        } catch (SQLException | InterruptedException e) {
            logger.log(Level.ERROR, e.getMessage());
        }
    }

    public static ConnectionPool getInstance() {
        lock.lock();
        if (instance == null) {
            instance = new ConnectionPool();
        }

        lock.unlock();
        return instance;
    }

    private void paramInit() {
        InputStream inputStream = ConnectionPool.class.getClassLoader().getResourceAsStream("connection.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            logger.log(Level.ERROR, e.getMessage());
        }
        this.jdbcUrl = properties.getProperty("jdbcUrl");
        this.userName = properties.getProperty("username");
        this.password = properties.getProperty("password");
        this.poolSize = Integer.parseInt(properties.getProperty("poolSize"));
    }

    public Connection getConnection() {
        ConnectionHandler handler = null;
        try {
            handler = new ConnectionHandler(connections.take());
        } catch (Exception e) {
            throw new ConnectionException("Exception in method: getConnection: " + e.getMessage());
        }
        return (Connection) Proxy.newProxyInstance(ConnectionPool.class.getClassLoader(), new Class[]
                {Connection.class}, handler);
    }

    public void returnConnection(Connection connection) throws SQLException, InterruptedException {
        if (!connection.isClosed()) {
            connections.put(connection);
        }
    }

    public void closeConnection(Connection connection) throws SQLException {
        if (!connection.isClosed()) {
            connection.close();
        }
    }

    public void closeAllConnections() throws InterruptedException, SQLException {
        while (!connections.isEmpty()) {
            connections.take().close();
        }
    }

    private Connection newConnection() throws SQLException {
        return DriverManager.getConnection(jdbcUrl, userName, password);
    }
}