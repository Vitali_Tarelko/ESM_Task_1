create SCHEMA "GiftCertificates";
-- Table: "GiftCertificates".tags

-- DROP TABLE "GiftCertificates".tags;

CREATE TABLE "GiftCertificates".tags
(
    id serial NOT NULL,
    name text COLLATE pg_catalog."default",
    CONSTRAINT tags_pkey PRIMARY KEY (id),
    CONSTRAINT name UNIQUE (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE "GiftCertificates".tags
    OWNER to postgres;

INSERT INTO "GiftCertificates".tags (name) VALUES ('malesuada'),('interdum'),('diam'),('ornare,'),('nascetur'),('mauris,'),('mi'),('lacus.'),('blandit'),('Nulla');


DROP TABLE IF EXISTS "GiftCertificates".certificates;

CREATE TABLE "GiftCertificates".certificates
(
    id serial NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default",
    price numeric(8,2) NOT NULL,
    creation_date date NOT NULL,
    modification_date date NOT NULL,
    duration integer NOT NULL,
    CONSTRAINT certificates_pkey PRIMARY KEY (id),
    CONSTRAINT certificates_name_key UNIQUE (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE "GiftCertificates".certificates
    OWNER to postgres;

INSERT INTO "GiftCertificates".certificates (name,description,price,creation_date,modification_date,duration) VALUES ('egestas. Aliquam fringilla cursus purus.','convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc','5.10','2018-02-05','2018-03-05',67),('in aliquet lobortis, nisi nibh','diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec,','0.96','2018-05-29','2018-06-09',91),('penatibus et magnis dis parturient','Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris,','9.35','2017-10-13','2017-09-28',96),('enim mi tempor lorem, eget','dictum placerat, augue. Sed molestie. Sed id risus quis diam','7.47','2017-11-10','2018-01-05',115),('Maecenas malesuada fringilla est. Mauris','ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna.','5.21','2018-05-12','2017-08-03',162),('ante, iaculis nec, eleifend non,','Morbi neque tellus, imperdiet non, vestibulum nec, euismod in, dolor.','7.64','2017-12-29','2017-08-30',41),('nostra, per inceptos hymenaeos. Mauris','mauris a nunc. In at pede. Cras vulputate velit eu','6.12','2018-05-09','2018-01-29',52),('scelerisque, lorem ipsum sodales purus,','tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a','5.90','2017-10-09','2017-10-23',176),('parturient montes, nascetur ridiculus mus.','vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras','5.24','2018-06-08','2018-07-13',113),('congue, elit sed consequat auctor,','nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras','2.21','2018-06-24','2018-02-13',149);



CREATE TABLE "GiftCertificates".tag_certificate
(
    tag_id integer NOT NULL,
    certificate_id integer NOT NULL,
    CONSTRAINT pk PRIMARY KEY (tag_id, certificate_id),
    CONSTRAINT tag_certificate_tag_id_fkey FOREIGN KEY (tag_id)
        REFERENCES "GiftCertificates".tags (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT tag_certificate_certificate_id_fkey FOREIGN KEY (certificate_id)
        REFERENCES "GiftCertificates".certificates (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

INSERT INTO "GiftCertificates".tag_certificate (tag_id,certificate_id) VALUES (7,3),(7,6),(3,2),(2,1),(2,7),(2,8),(7,5),(9,10),(8,8);
INSERT INTO "GiftCertificates".tag_certificate (tag_id,certificate_id) VALUES (9,8),(7,2),(5,9),(9,5),(8,7),(3,3),(8,1);
INSERT INTO "GiftCertificates".tag_certificate (tag_id,certificate_id) VALUES (3,5),(8,3),(2,2),(3,4),(7,1),(9,3),(3,8),(10,4),(8,10);

DROP FUNCTION "GiftCertificates".find_by_text(text);
CREATE  OR REPLACE FUNCTION "GiftCertificates".find_by_text (string text)
RETURNS TABLE (id integer, name text, description text, price numeric, creation_date date, modification_date date, duration integer, tag_name text[], tag_id integer[])
AS $$
        BEGIN
               RETURN QUERY Select "GiftCertificates".certificates.id, "GiftCertificates".certificates.name, "GiftCertificates".certificates.description,
			   "GiftCertificates".certificates.price, "GiftCertificates".certificates.creation_date,
			   "GiftCertificates".certificates.modification_date, "GiftCertificates".certificates.duration,
			   array_agg("GiftCertificates".tags.name),  array_agg("GiftCertificates".tags.id)
			   from "GiftCertificates".certificates left join "GiftCertificates".tag_certificate on certificate_id="GiftCertificates".certificates.id
			   LEFT JOIN "GiftCertificates".tags on "GiftCertificates".tags.id="GiftCertificates".tag_certificate.tag_id
			   where "GiftCertificates".certificates.name  ILIKE '%' ||  $1 || '%'
			   OR "GiftCertificates".certificates.description  ILIKE '%' ||  $1 || '%'
			   GROUP BY "GiftCertificates".certificates.id;
        END;
$$ LANGUAGE plpgsql;
CREATE  OR REPLACE FUNCTION "GiftCertificates".find_by_text_and_tag (string text, tag text)
RETURNS TABLE (id integer, name text, description text, price numeric, creation_date date, modification_date date, duration integer, tag_name text[], tag_id integer[])
AS $$
        BEGIN
               RETURN QUERY Select "GiftCertificates".certificates.id, "GiftCertificates".certificates.name, "GiftCertificates".certificates.description,
			   "GiftCertificates".certificates.price, "GiftCertificates".certificates.creation_date,
			   "GiftCertificates".certificates.modification_date, "GiftCertificates".certificates.duration,
			   array_agg("GiftCertificates".tags.name),  array_agg("GiftCertificates".tags.id)
			from "GiftCertificates".certificates left join "GiftCertificates".tag_certificate on certificate_id="GiftCertificates".certificates.id
			   LEFT JOIN "GiftCertificates".tags on "GiftCertificates".tags.id="GiftCertificates".tag_certificate.tag_id
			   where "GiftCertificates".tags.name = $2 and ("GiftCertificates".certificates.name  ILIKE '%' ||  $1 || '%'
			   OR "GiftCertificates".certificates.description  ILIKE '%' ||  $1 || '%' )
			   GROUP BY "GiftCertificates".certificates.id;
        END;
$$ LANGUAGE plpgsql;

DROP FUNCTION "GiftCertificates".find_by_tag(text);
CREATE  OR REPLACE FUNCTION "GiftCertificates".find_by_tag (string text)
RETURNS TABLE (id integer, name text, description text, price numeric, creation_date date, modification_date date, duration integer, tag_name text[], tag_id integer[])
AS $$
        BEGIN
               RETURN QUERY Select "GiftCertificates".certificates.id, "GiftCertificates".certificates.name, "GiftCertificates".certificates.description,
			   "GiftCertificates".certificates.price, "GiftCertificates".certificates.creation_date,
			   "GiftCertificates".certificates.modification_date, "GiftCertificates".certificates.duration,
			   array_agg("GiftCertificates".tags.name),  array_agg("GiftCertificates".tags.id)
			   from "GiftCertificates".certificates left join "GiftCertificates".tag_certificate on certificate_id="GiftCertificates".certificates.id
			   LEFT JOIN "GiftCertificates".tags on "GiftCertificates".tags.id="GiftCertificates".tag_certificate.tag_id
			   where "GiftCertificates".tags.name = $1
			   GROUP BY "GiftCertificates".certificates.id;
        END;
$$ LANGUAGE plpgsql;