package com.epam.esm.service;

import com.epam.esm.domain.Tag;
import com.epam.esm.repository.AbstractRepository;
import com.epam.esm.repository.specification.TagSpecification;
import com.epam.esm.service.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class TagService {
    private static final String DATA_NOT_FOUND = "Data not found";
    @Autowired
    @Qualifier("tagRepository")
    private AbstractRepository repository;


    /**
     * Transactional method accept new Tag object,
     * using tagRepository bean create new Tag.
     *
     * @param entity is an object that will be added to the database.
     * @return Tag if the record was added successful.
     * @throws org.springframework.dao.DuplicateKeyException
     */
    @Transactional
    public Tag create(Tag entity) {
        int id = repository.create(entity);
        return (Tag) repository.query(new TagSpecification.QueryBuilder().byId(id).buildQuery()).get(0);
    }

    /**
     * Method which remove Tag from database.
     * All dependencies between removed object and GiftCertificates well be also removed.
     *
     * @param id is the Tag primary key in the database.
     * @return true if the record was removed successful.
     * @throws NotFoundException
     */
    public boolean removeById(int id) {
        if (repository.remove(id)) {
            return true;
        }
        throw new NotFoundException(DATA_NOT_FOUND);
    }

    /**
     * Method return existing Tag by id from database.
     *
     * @param id is the Tag primary key in the database.
     * @return Tag
     * @throws NotFoundException if tag not found
     */
    public Tag getById(int id) {
        List<Tag> result = repository.query(new TagSpecification.QueryBuilder().byId(id).buildQuery());
        if (!result.isEmpty()) {
            return result.get(0);
        }
        throw new NotFoundException(DATA_NOT_FOUND);
    }

    /**
     * Method return all Tags from database.
     *
     * @return List<Tags> list with Tag
     * @throws NotFoundException if no tags found
     */
    public List<Tag> getAll() {
        List<Tag> result = repository.query(new TagSpecification.QueryBuilder().buildQuery());
        if (!result.isEmpty()) {
            return result;
        }
        throw new NotFoundException(DATA_NOT_FOUND);
    }
}
