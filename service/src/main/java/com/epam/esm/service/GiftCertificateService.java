package com.epam.esm.service;

import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.domain.ParametersTransfer;
import com.epam.esm.domain.Tag;
import com.epam.esm.repository.AbstractRepository;
import com.epam.esm.repository.specification.CertificateSpecification;
import com.epam.esm.repository.specification.Specification;
import com.epam.esm.repository.specification.TagSpecification;
import com.epam.esm.service.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class GiftCertificateService {
    private static final String DATA_NOT_FOUND = "Data not found";
    private static final String NOT_FOUND_BY_ID = "Not found Certificate by id: ";
    private static final String ORDER_TYPE_NAME = "name";
    private static final String ORDER_TYPE_DATE = "date";
    @Autowired
    @Qualifier("giftCertificateRepository")
    private AbstractRepository certificateRepository;
    @Autowired
    @Qualifier("tagRepository")
    private AbstractRepository tagRepository;

    /**
     * Transactional method accept new GiftCertificate object,
     * using certificateRepository bean create new GiftCertificate and
     * then using tagRepository create dependency between certificate and tags.
     * If new tags are passed during creation they will be created in db.
     *
     * @param entity is an object that will be added to the database.
     * @return GiftCertificate if the record was added successful.
     * @throws org.springframework.dao.DuplicateKeyException
     * @throws NotFoundException
     */
    @Transactional
    public GiftCertificate create(GiftCertificate entity) {
        int id = certificateRepository.create(entity);
        if (entity.getTagList() != null) {
            entity.getTagList().forEach(tag -> updateTagCertificate(tag, id));
        }
        List<GiftCertificate> result = certificateRepository
                .query(new CertificateSpecification.QueryBuilder().byId(id)
                        .groupBy().buildQuery());
        if (!result.isEmpty()) {
            return result.get(0);
        }
        throw new NotFoundException(DATA_NOT_FOUND);
    }

    /**
     * Method which remove GiftCertificate from database.
     * All dependencies between remove object and tags well be also removed.
     *
     * @param id is the GiftCertificate primary key in the database.
     * @return true if the record was removed successful.
     * @throws NotFoundException
     */
    public boolean removeById(int id) {
        if (certificateRepository.remove(id)) {
            return true;
        }
        throw new NotFoundException(NOT_FOUND_BY_ID + id);
    }

    /**
     * Method return existing GiftCertificate by id from database.
     *
     * @param id is the GiftCertificate primary key in the database.
     * @return GiftCertificate
     * @throws NotFoundException if certificate not found
     */
    public GiftCertificate getById(int id) {
        List<GiftCertificate> result = certificateRepository
                .query(new CertificateSpecification.QueryBuilder().byId(id)
                        .groupBy().buildQuery());
        if (!result.isEmpty()) {
            return result.get(0);
        }
        throw new NotFoundException(DATA_NOT_FOUND);
    }

    /**
     * Method return all GiftCertificates from database.
     *
     * @return List<GiftCertificate> list with GiftCertificates
     * @throws NotFoundException if no certificates found
     */
    public List<GiftCertificate> getAll() {
        List<GiftCertificate> result = certificateRepository.query(new
                CertificateSpecification.QueryBuilder().groupBy().buildQuery());
        if (!result.isEmpty()) {
            return result;
        }
        throw new NotFoundException(DATA_NOT_FOUND);
    }

    /**
     * Transactional method accept existing GiftCertificate object,
     * using certificateRepository bean update object and
     * then using tagRepository create dependency between certificate and tags.
     * If new tags are passed during modification they will be created in db.
     *
     * @param entity is an object that will be update.
     * @return GiftCertificate if the successfuly updated object.
     * @throws org.springframework.dao.DuplicateKeyException
     * @throws NotFoundException if certificate with such id not exist.
     */
    @Transactional
    public GiftCertificate update(GiftCertificate entity) {
        if (certificateRepository.update(entity)) {
            tagRepository.removeTagCertificateBy(entity.getId());
            if (entity.getTagList() != null) {
                entity.getTagList().forEach(tag -> updateTagCertificate(tag, entity.getId()));
            }
            return (GiftCertificate) certificateRepository.query(new CertificateSpecification.QueryBuilder().byId
                    (entity.getId())
                    .groupBy().buildQuery()).get(0);
        }
        throw new NotFoundException(NOT_FOUND_BY_ID + entity.getId());
    }

    /**
     * Method return all GiftCertificates from database which
     * fit input parameters from ParameterTransfer object.
     *
     * @param parameters object, which contains parameters for searching.
     * @return List<GiftCertificate> list with GiftCertificates.
     * @throws NotFoundException if no certificates found.
     * @see com.epam.esm.domain.ParametersTransfer
     */
    public List<GiftCertificate> getByParameters(ParametersTransfer parameters) {
        List<GiftCertificate> certificates = certificateRepository.query(buildSpecification(parameters));
        if (!certificates.isEmpty()) {
            return certificates;
        }
        throw new NotFoundException(DATA_NOT_FOUND);
    }

    private void updateTagCertificate(Tag tag, int id) {
        List<Tag> tags = tagRepository.query(new TagSpecification.QueryBuilder().byName(tag.getName()).buildQuery());
        if (!tags.isEmpty()) {
            tagRepository.createDependency(tags.get(0).getId(), id);
        } else {
            int newTagId = tagRepository.create(tag);
            Tag newTag =
                    (Tag) tagRepository.query(new TagSpecification.QueryBuilder().byId(newTagId).buildQuery()).get(0);
            tagRepository.createDependency(newTag.getId(), id);
        }
    }

    private Specification buildSpecification(ParametersTransfer parameters) {
        CertificateSpecification.QueryBuilder builder = new CertificateSpecification.QueryBuilder();

        if (parameters.getTag() != null) {
            builder.byTag(parameters.getTag());
        }
        if (parameters.getText() != null) {
            builder.byText(parameters.getText());
        }

        if (parameters.getText() != null && parameters.getTag() != null) {
            builder.byTagAndText(parameters.getText(), parameters.getTag());
        }
        if (parameters.getText() == null && parameters.getTag() == null) {
            builder.groupBy();
        }
        if (ORDER_TYPE_NAME.equals(parameters.getOrder())) {
            builder.orderByName();
        }
        if (ORDER_TYPE_DATE.equals(parameters.getOrder())) {
            builder.orderByModificationDate();
        }
        return builder.buildQuery();
    }
}
