package com.epam.esm.service;

import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.domain.ParametersTransfer;
import com.epam.esm.repository.GiftCertificateRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.service.exception.NotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GiftCertificateServiceTest {

    private static final Logger LOGGER = LogManager.getLogger(GiftCertificateService.class);
    private static List<GiftCertificate> byIdResult;
    private static List<GiftCertificate> allResult;
    private static ParametersTransfer parameters;
    @Mock
    private GiftCertificateRepository certificateRepository;
    @Mock
    private TagRepository tagRepository;
    @InjectMocks
    private GiftCertificateService service;

    @BeforeClass
    public static void setUp() {
        allResult = new ArrayList<>();
        allResult.add(new GiftCertificate.Builder("FirstCertificate").buildCertificate());
        allResult.add(new GiftCertificate.Builder("SecondCertificate").buildCertificate());
        byIdResult = new ArrayList<>();
        byIdResult.add(new GiftCertificate.Builder("ByIdResult").withId(1).buildCertificate());
        parameters = new ParametersTransfer();
        parameters.setText("text");
        parameters.setTag("teg");
        parameters.setOrder("date");
    }

    @AfterClass
    public static void tearDown() {
        allResult = null;
        byIdResult = null;
    }

    @Test
    public void create() {
        GiftCertificate expected = byIdResult.get(0);
        when(certificateRepository.create(any())).thenReturn(1);
        when(certificateRepository.query(any())).thenReturn(byIdResult);
        GiftCertificate result = service.create(new GiftCertificate.Builder("new").buildCertificate());
        assertEquals(expected, result);
        LOGGER.log(Level.INFO, "Expected: " + expected.toString() + " Result: " + result.toString());
        verify(certificateRepository, times(1)).create(any());
        verify(certificateRepository, times(1)).query(any());
    }

    @Test(expected = NotFoundException.class)
    public void createException() {
        when(certificateRepository.create(any())).thenReturn(1);
        when(certificateRepository.query(any())).thenReturn(new ArrayList<>());
        service.create(new GiftCertificate.Builder("new").buildCertificate());
        LOGGER.log(Level.INFO, "Expected Exception: NotFoundException.class");

    }

    @Test
    public void removeByIdTest() {
        when(certificateRepository.remove(any())).thenReturn(true);
        boolean result = service.removeById(1);
        assertEquals(true, result);
    }

    @Test(expected = NotFoundException.class)
    public void removeByIdExceptionTest() {
        when(certificateRepository.remove(any())).thenReturn(false);
        service.removeById(1);
    }

    @Test
    public void getById() {
        when(certificateRepository.query(any())).thenReturn(byIdResult);
        GiftCertificate result = service.getById(2);
        LOGGER.log(Level.INFO, result.toString());
        assertEquals(byIdResult.get(0), result);
    }

    @Test(expected = NotFoundException.class)
    public void getByIdNotFound() {
        when(certificateRepository.query(any())).thenReturn(new ArrayList<>());
        service.getById(2);
    }

    @Test
    public void getAll() {
        when(certificateRepository.query(any())).thenReturn(allResult);
        List<GiftCertificate> result = service.getAll();
        assertEquals(allResult.size(), result.size());
    }

    @Test(expected = NotFoundException.class)
    public void getAllNotFound() {
        when(certificateRepository.query(any())).thenReturn(new ArrayList<>());
        service.getAll();
    }

    @Test
    public void update() {
        when(certificateRepository.update(any())).thenReturn(true);
        when(certificateRepository.query(any())).thenReturn(byIdResult);
        service.update(byIdResult.get(0));
        verify(certificateRepository, times(1)).update(any());
        verify(certificateRepository, times(1)).query(any());
    }

    @Test(expected = NotFoundException.class)
    public void updateException() {
        when(certificateRepository.update(any())).thenReturn(false);
        service.update(byIdResult.get(0));
    }

    @Test
    public void getByParameters() {
        when(certificateRepository.query(any())).thenReturn(allResult);
        List<GiftCertificate> result = service.getByParameters(parameters);
        assertEquals(allResult, result);
        verify(certificateRepository, times(1)).query(any());
    }
}