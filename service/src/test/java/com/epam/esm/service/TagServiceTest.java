package com.epam.esm.service;

import com.epam.esm.domain.Tag;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.service.exception.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
    private static final Logger LOGGER = LogManager.getLogger(TagServiceTest.class);

    @Mock
    private TagRepository repository;
    @InjectMocks
    private TagService service;

    private static List<Tag> getByIdResult;
    private static List<Tag> getAllResult;

    @BeforeClass
    public static void setUp() {
        getByIdResult = new ArrayList<>();
        getAllResult = new ArrayList<>();
        getByIdResult.add(new Tag(1, "Tag1"));
        getAllResult.add(new Tag(1, "Tag1"));
        getAllResult.add(new Tag(2, "Tag2"));
        getAllResult.add(new Tag(3, "Tag3"));
    }

    @AfterClass
    public static void tearDown() {
        getByIdResult = null;
        getAllResult = null;
    }

    @Test
    public void createTagTest() {
        Tag expected = getByIdResult.get(0);
        when(repository.create(any())).thenReturn(1);
        when(repository.query(any())).thenReturn(getByIdResult);
        Tag result = service.create(expected);
        LOGGER.info("Expected tag: " + expected.toString() + ", Result is: " + result.toString());
        assertEquals(expected, result);
        verify(repository, times(1)).create(any());
        verify(repository, times(1)).query(any());
    }

    @Test
    public void removeByIdTest() {
        when(repository.remove(any())).thenReturn(true);
        boolean result = service.removeById(1);
        assertEquals(true, result);
    }

    @Test(expected = NotFoundException.class)
    public void removeByIdExceptionTest() {
        when(repository.remove(any())).thenReturn(false);
        service.removeById(1);
    }


    @Test
    public void getById() {
        when(repository.query(any())).thenReturn(getByIdResult);
        Tag result = service.getById(2);
        LOGGER.info(result.toString());
        assertEquals(getByIdResult.get(0), result);
    }

    @Test(expected = NotFoundException.class)
    public void getTagByIdException() {
        when(repository.query(any())).thenReturn(new ArrayList<>());
        service.getById(1);
    }

    @Test
    public void getAll() {
        when(repository.query(any())).thenReturn(getAllResult);
        List<Tag> result = service.getAll();
        result.forEach(tag -> LOGGER.info(tag.toString()));
        assertEquals(getAllResult.size(), result.size());
    }

    @Test(expected = NotFoundException.class)
    public void getAllTagExceptionMsg() {
        when(repository.query(any())).thenReturn(new ArrayList<>());
        service.getAll();
    }

}