package com.epam.esm.web.exception;

import org.springframework.http.HttpStatus;

import java.util.Objects;

public class Error {

    private HttpStatus status;
    private String message;

    public Error(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Error error = (Error) o;
        return status == error.status &&
                Objects.equals(message, error.message);
    }

    @Override
    public int hashCode() {

        return Objects.hash(status, message);
    }

    @Override
    public String toString() {
        return "Error{" +
                "status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
