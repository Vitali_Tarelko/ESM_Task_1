package com.epam.esm.web;

import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.domain.ParametersTransfer;
import com.epam.esm.service.GiftCertificateService;
import org.omg.CORBA.DATA_CONVERSION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/esm/certificates")
public class GiftCertificateController {

    @Autowired
    private GiftCertificateService service;

    @GetMapping("/")
    @ResponseBody
    public ResponseEntity<List<GiftCertificate>> getAllCertificates() {
        List<GiftCertificate> list = service.getAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<GiftCertificate> getCertificateById(@PathVariable int id) {
        GiftCertificate certificate = service.getById(id);
        return new ResponseEntity<>(certificate, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<String> deleteCertificate(@PathVariable int id) {
        service.removeById(id);
        return new ResponseEntity<>("GiftCertificate successfully removed.", HttpStatus.OK);

    }

    @PutMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<GiftCertificate> updateCertificate(@PathVariable int id,
                                                             @RequestBody @Valid GiftCertificate certificate) {
        GiftCertificate current = service.getById(id);
        current.setName(certificate.getName());
        current.setDescription(certificate.getDescription());
        current.setDuration(certificate.getDuration());
        current.setPrice(certificate.getPrice());
        current.setModificationDate(LocalDate.now());
        current.setTagList(certificate.getTagList());
        GiftCertificate updated = service.update(current);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<GiftCertificate> createCertificate(@RequestBody @Valid GiftCertificate certificate) {
        certificate.setCreationDate(LocalDate.now());
        certificate.setModificationDate(LocalDate.now());
        GiftCertificate result = service.create(certificate);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<GiftCertificate>> findCertificate(@RequestParam(value = "text", required = false)
                                                                         String text,
                                                                 @RequestParam(value = "tag", required = false)
                                                                         String tag,
                                                                 @RequestParam(value = "order", required = false)
                                                                         String order) {

        List<GiftCertificate> result = service.getByParameters(createTransferParameters(text, tag, order));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    private ParametersTransfer createTransferParameters(String text, String tag, String order) {
        ParametersTransfer parameters = new ParametersTransfer();
        if (text != null) {
            parameters.setText(text);
        }
        if (tag != null) {
            parameters.setTag(tag);
        }
        if ("name".equals(order)) {
            parameters.setOrder("name");
        }
        if ("date".equals(order)) {
            parameters.setOrder("date");
        }
        return parameters;
    }
}
