package com.epam.esm.web;

import com.epam.esm.domain.Tag;
import com.epam.esm.service.TagService;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("esm/tags")
public class TagController {

    @Autowired
    private TagService service;

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Tag>> getAllTag() {
        List<Tag> tags = service.getAll();
        return new ResponseEntity<>(tags, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Tag> getTagById(@PathVariable int id) {
        Tag tag = service.getById((id));
        return new ResponseEntity<>(tag, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<String> deleteCertificate(@PathVariable int id) {
        service.removeById(id);
        return new ResponseEntity<>("Tag successfully removed.", HttpStatus.OK);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Tag> createTag(@RequestBody Tag tag) {
        Tag created = service.create(tag);
        return new ResponseEntity<>(created, HttpStatus.CREATED);
    }

}
