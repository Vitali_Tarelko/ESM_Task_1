package com.epam.esm.web.exception;

import com.epam.esm.service.exception.NotFoundException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;


@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<Object> handleNotFound(Exception ex, WebRequest request) {
        Locale locale = request.getLocale();
        Error error = new Error(HttpStatus.NOT_FOUND, messageSource.getMessage("notFound", null, locale));
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DuplicateKeyException.class)
    protected ResponseEntity<Object> handleDuplicateException(Exception ex, WebRequest request) {
        Locale locale = request.getLocale();
        Error error = new Error(HttpStatus.CONFLICT, messageSource.getMessage("alreadyExist", null, locale));
        return new ResponseEntity<>(error, HttpStatus.CONFLICT);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest
                                                                          request) {
        Locale locale = request.getLocale();
        BindingResult result = ex.getBindingResult();
        List<String> errorMessages = result.getAllErrors()
                .stream()
                .map(objectError -> messageSource.getMessage(objectError, locale))
                .collect(Collectors.toList());
        Error error = new Error(HttpStatus.BAD_REQUEST, errorMessages.get(0));
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus
            status, WebRequest request) {
        Locale locale = request.getLocale();
        Error error = new Error(HttpStatus.BAD_REQUEST, messageSource.getMessage("typeMismatch", null, locale));
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleOtherException(Exception ex, WebRequest request) {
        Locale locale = request.getLocale();
        Error error = new Error(HttpStatus.BAD_REQUEST, messageSource.getMessage("otherException", null, locale));
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }


}

